<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormAddRequest;
use App\Http\Requests\FormEditRequest;
use App\Http\Requests\FormRequest;
use App\Jobs\UrlJob;
use App\Models\UrlModel;
use http\Url;
use Illuminate\Http\Request;

class UrlContorller extends Controller
{
    public function job()
    {
        $items=UrlModel::withoutTrashed()->get();
        foreach($items as $item){
            UrlJob::dispatch($item);
        }
    }

    public function index()
    {
        $result=[
          'data'=>UrlModel::withoutTrashed()->get(),
        ];
        return view('url.index',compact('result'));
    }

    public function add()
    {
        return view('url.form');
    }

    public function addPr(FormAddRequest $request)
    {
        $url = new UrlModel();
        $url->name = $request->name;
        $url->url = $request->url;
        $url->timeStart = $request->timeStart;
        $url->save();

        $result = [
            'message'=>__('text.successAddUrl'),
            'class'=>'success'
        ];
        return view('url.form',compact('result'));
    }

    public function edit(UrlModel $id)
    {
        $result=[
          'data'=>$id
        ];
        return view('url.form',compact('result'));
    }
    public function editPr(FormEditRequest $request)
    {
        $id = $request->id;
        $checkUrl = UrlModel::withoutTrashed()->where('id','=',$id)->get();

        if($checkUrl->isNotEmpty()) {
            $checkUrl->toQuery()->update([
                'name'=>$request->name,
                'url'=>$request->url,
                'timeStart'=>$request->timeStart
            ]);

            $result = [
                'data'=>$checkUrl->toQuery()->get()->first(),
                'message' => __('text.successUpdateUrl'),
                'class' => 'success'
            ];
        }else{
            $result = [
                'message' => __('text.wornUrlNotFound'),
                'class' => 'danger'
            ];
        }
        return view('url.form',compact('result'));
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $checkUrl = UrlModel::withoutTrashed()->where('id','=',$id)->get();

        if($checkUrl->isNotEmpty()) {
            $checkUrl->toQuery()->forceDelete();

            $result = [
                'data'=>$checkUrl->toQuery()->get()->first(),
                'message' => __('text.successDeleteUrl'),
                'class' => 'success'
            ];
        }else{
            $result = [
                'message' => __('text.wornUrlNotFound'),
                'class' => 'danger'
            ];
        }
        return view('url.index',compact('result'));
    }
}
