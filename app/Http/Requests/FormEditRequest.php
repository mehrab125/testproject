<?php

namespace App\Http\Requests;


class FormEditRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'url'=>'required|url|max:255',
//            'timeStart'=>'required'
        ];
    }
    public function messages()
    {
        return [
          'name.required'=>__('text.warnNameRequired'),
          'name.max'=>__('text.warnNameMax'),
          'url.required'=>__('text.warnUrlRequired'),
          'url.max'=>__('text.warnUrlMax'),
          'url.url'=>__('text.warnUrlUrl'),
//          'timeStart.required'=>__('text.warnTimeStartRequired')
        ];
    }
}
