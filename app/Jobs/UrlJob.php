<?php

namespace App\Jobs;

use App\Models\FormModel;
use App\Models\UrlModel;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class UrlJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UrlModel $url)
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::get($this->url->url);
        if (!empty($response) && $response->status()==200) {
            UrlModel::withTrashed()->where('id','=',$this->url->id)->update([
                'lastTimeStart'=>Carbon::now()
            ]);

            FormModel::withTrashed()->where('url_id', '=', $this->url->id)->forceDelete();

            $items=$response->object();
            switch ($this->url->id) {
                case 1:
                    foreach ($items as $item) {
                        $form = new FormModel();
                        $form->url_id = $this->url->id;
                        $form->ticketID = $item->TicketID;
                        $form->origin = $item->Origin;
                        $form->destination = $item->Destination;
                        $form->flightDate = $item->FlightDate;
                        $form->flightNumber = $item->FlightNumber;
                        $form->airline = $item->Airline;
                        $form->price = $item->Price;
                        $form->count = $item->Count;
                        $form->save();
                    }
                    break;
                case 2:
                    foreach ($items as $item) {
                        $form = new FormModel();
                        $form->url_id = $this->url->id;
                        $form->ticketID = $item->ticket_id;
                        $form->origin = $item->origin;
                        $form->destination = $item->destination;
                        $form->flightDate = $item->date;
                        $form->flightNumber = $item->flight_number;
                        $form->airline = $item->airline;
                        $form->price = $item->price;
                        $form->count = $item->count;
                        $form->save();
                    }
                    break;
            }
        }
    }
}
