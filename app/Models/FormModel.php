<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='form';

    public function urlModel(){
        return $this->belongsTo(UrlModel::class,'url_id','id');
    }
}
