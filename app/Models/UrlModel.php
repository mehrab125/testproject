<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UrlModel extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table='url';


    public function FormModel(){
        return $this->hasMany(FormModel::class,'id','url_id');
    }
}
