<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("url_id")->nullable()->comment("ردیف آدرس");
            $table->string("ticketID")->nullable()->comment('ردیف تیکت');
            $table->string('origin')->nullable()->comment('مبدا');
            $table->string('destination')->nullable()->comment('مقصد');
            $table->string('flightDate')->nullable()->comment('زمان پرواز');
            $table->integer('flightNumber')->nullable()->comment('شماره پرواز');
            $table->string('airline')->nullable()->comment('airline');
            $table->integer('price')->nullable()->comment('قیمت');
            $table->integer('count')->nullable()->comment('تعداد');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
