<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable()->comment('نام');
            $table->string('url')->nullable()->comment('آدرس');
            $table->string('token')->nullable()->comment('توکن');
            $table->string('timeStart')->nullable()->comment('زمان اجرا');
            $table->dateTime('lastTimeStart')->nullable()->comment('اخرین زمان اجرا');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
