<?php
return [
    'id' => 'ردیف',
    'welcome' => 'خوش آمدید',
    'panelAdmin' => 'پنل مدیریت ',
    'forgetPassword' => 'فراموشی رمزعبور',
    'login' => 'ورود',
    'register' => 'ثبت نام',
    'createAccount' => 'ایجاد حساب کاربری',
    'nameShop' => 'نام فروشگاه',
    'supplier' => 'تامین کننده',
    'addSupplier'=>'افزودن تامین کننده',
    'editSupplier'=>'ویرایش تامین کننده',
    'trashSupplier'=>'تامین کننده حذف شده',
    'name' => 'نام',
    'lastName' => 'نام خانوادگی',
    'phone' => 'شماره تلفن همراه',
    'email' => 'ایمیل',
    'address' => 'آدرس',
    'postalCode' => 'کد پستی',
    'payment' => 'کیف پول',
    'typePrice' => 'تومان',
    'time' => 'زمان',
    'date' => 'تاریخ',
    'dateIn' => 'از تاریخ',
    'dateOut' => 'تا تاریخ',
    'username' => 'نام کاربری',
    'password' => 'رمز عبور',
    'confirmPassword' => 'رمز عبور (مجدد)',
    'rules' => 'قوانین',
    'recoveryPassword' => 'بازیابی رمزعبور',
    'codeRecovery' => 'کد بازیابی',
    'codeRecoveryPassword' => 'کد بازیابی رمزعبور',
    'admin' => 'مدیر',
    'user' => 'کاربر',
    'profile' => 'پروفایل',
    'changePassword' => 'تعویض رمزعبور',
    'editProfile' => 'ویرایش پروفایل',
    'editUser' => 'ویرایش کاربر',
    'editRoles' => 'ویرایش دسترسی',
    'logout' => 'خروج',
    'dashboard' => 'داشبورد',
    'inProcessing' => 'درحال پردازش...',
    'viewData' => 'نمایش محتویات',
    'item' => 'مورد',
    'ofTotal' => 'از مجموع',
    'view' => 'بازدید',
    'to' => 'تا',
    'zero' => 'تهی',
    'filterTotal' => 'فیلتر شده از مجموع',
    'search' => 'جستجو',
    'first' => 'ابتدا',
    'previous' => 'قبلی',
    'next' => 'بعدی',
    'last' => 'انتها',
    'rank' => 'جایگاه',
    'lastUpdate' => 'آخرین بروزرسانی',
    'users' => 'کاربران',
    'usersTrash' => 'کاربران حذف شده',
    'usersRoles' => 'دسترسی کاربران',
    'roles' => 'دسترسی',
    'rolesAdd' => 'افزودن دسترسی',
    'delete' => 'حذف',
    'edit' => 'ویرایش',
    'add' => 'افزودن',
    'inactive' => 'غیرفعال',
    'active' => 'فعال',
    'details' => 'جزئیات',
    'recovery' => 'بازیابی',
    'deleteTrash' => 'حذف',
    'options' => 'تنظیمات',
    'back' => 'برگشت',
    'pages' => 'صفحات',
    'page' => 'صفحه',
    'send' => 'ارسال',
    'pageAll' => 'تمام صفحات',
    'trash' => 'سطل زباله',
    'selectRoles' => 'انتخاب دسترسی ...',
    'product'=>'محصول',
    'addProduct'=>'افزودن محصول',
    'editProduct'=>'ویرایش محصول',
    'trashProduct'=>'محصولات حذف شده',
    'orders'=>'سفارش',
    'noPayment'=>'پرداخت نشده',
    'ordersNoPayment'=>'سفارشات پرداخت نشده',
    'ordersNoSend'=>'سفارشات ارسال نشده',
    'addOrders'=>'افزودن سفارش',
    'editOrders'=>'ویرایش سفارش',
    'trashOrders'=>'سفارشات حذف شده',
    'post'=>'پست',
    'addPost'=>'افزودن پست',
    'editPost'=>'ویرایش پست',
    'trashPost'=>'پستات حذف شده',
    'percentDiscount'=>'درصد تخفیف',
    'priceColleague'=>'قیمت همکار',
    'price'=>'قیمت',
    'priceSell'=>'قیمت فروش',
    'priceBuy'=>'قیمت خرید',
    'vipProduct'=>'محصول ویژه',
    'tags'=>'برچسب',
    'yes'=>'بله',
    'no'=>'خیر',
    'indexPage'=>'صفحه اصلی',
    'listPage'=>'صفحه لیست',
    'selectColor'=>'انتخاب رنگ',
    'category'=>'دسته',
    'addCategory'=>'افزودن دسته',
    'editCategory'=>'ویرایش دسته',
    'trashCategory'=>'دسته های حذف شده',
    'childCategory' => 'زیر دسته',
    'motherCategory' => 'دسته مادر',
    'form'=>'فرم',
    'addForm'=>'افزودن فرم',
    'editForm'=>'ویرایش فرم',
    'trashForm'=>'فرم های حذف شده',
    'timeStart'=>'زمان اجرا(هر 1 ساعت)',
    'lastTimeStart'=>'آخرین زمان اجرا',
    'url'=>'آدرس اینترنتی',
    'addUrl'=>'افزودن آدرس',
    'editUrl'=>'ویرایش آدرس',
    'trashUrl'=>'آدرس های حذف شده',
    'menu'=>'منو',
    'addMenu'=>'افزودن منو',
    'editMenu'=>'ویرایش منو',
    'trashMenu'=>'منو های حذف شده',
    'childMenu' => 'زیر منو',
    'motherMenu' => 'منو مادر',
    'brand'=>'برند',
    'addBrand'=>'افزودن برند',
    'editBrand'=>'ویرایش برند',
    'trashBrand'=>'برند های حذف شده',
    'childBrand' => 'زیر برند',
    'motherBrand' => 'برند مادر',
    'color'=>'رنگ',
    'addColor'=>'افزودن رنگ',
    'editColor'=>'ویرایش رنگ',
    'trashColor'=>'رنگ های حذف شده',
    'fields'=>'فیلد',
    'addFields'=>'افزودن فیلد',
    'editFields'=>'ویرایش فیلد',
    'trashFields'=>'فیلد های حذف شده',
    'comment' => 'نظر',
    'comments' => 'نظرات',
    'replayComment' => 'پاسخ به نظر',
    'editComment' => 'ویرایش نظر',
    'replay' => 'پاسخ',
    'trashComment' => 'نظرات حذف شده',
    'editSlider' => 'ویرایش اسلایدر',
    'slider' => 'اسلایدر',
    'addSlider'=>'افزودن اسلایدر',
    'editDiscount' => 'ویرایش تخفیف',
    'discount' => 'تخفیف',
    'addDiscount'=>'افزودن تخفیف',
    'trashDiscount'=>'تخفیف های حذف شده',
    'doesnt'=>'وارد نشده',
    'logo'=>'لگو',
    'status'=>'وضعیت',
    'dataIn'=>'از تاریخ',
    'dataOut'=>'تا تاریخ',
    'code'=>'کد',
    'countUse'=>'تعداد استفاده',
    'groupUser'=>'گروه کاربر',
    'typeDiscount'=>'نوع تخفیف',
    'minprice'=>'کمترین مبلغ',
    'maxprice'=>'بیشترین مبلغ',
    'editFooterUrl' => 'ویرایش پیوند پانوشته',
    'footerUrl' => 'پیوند پانوشته',
    'addFooterUrl'=>'افزودن پیوند پانوشته',
    'editAdvertising' => 'ویرایش تبلیغ',
    'advertising' => 'تبلیغ',
    'addAdvertising'=>'افزودن تبلیغ',
    'description'=>'توضیحات',
    'url'=>'آدرس اینترنتی',
    'image'=>'عکس',
    'title'=>'عنوان',
    'rankAssign'=>'اختصاص به جایگاه',
    'unknown'=>'ناشناس',
    'position'=>'وضعیت',
    'answerAdmin'=>'پاسخ مدیریت',
    'answerUser'=>'پاسخ کاربر',
    'closed'=>'بسته',
    'close'=>'بستن',
    'reply'=>'پاسخ',
    'branchCode'=>'كد شعبه',
    'branchName'=>'نام شعبه',
    'number'=>'تعداد',
    'invoiceAmount'=>'مبلغ فاكتور',
    'amount'=>'موجودی',
    'numbersCustomers'=>'تعداد مشتري',
    'factorHistory'=>'تاريخ فاكتور',
    'month'=>'ماه',
    'cashAmount'=>'مبلغ نقدي',
    'custcreditAmount'=>'مبلغ اعتباری',
    'poseAmount'=>'مبلغ پوز',
    'netReceived'=>'خالص دريافتي',
    'netSales'=>'خالص فروش',
    'net'=>'خالص',
    'remaining'=>'مانده',
    'discountAmount'=>'مبلغ تخفيف',
    'process'=>'روند',
    'returnAmount'=>'مبلغ برگشتی',
    'returnInvoice'=>'فاكتور برگشتی',
    'cashReturn'=>'برگشتی نقدی',
    'dataSettlement '=>'تاریخ تسویه',
    'totalDiscountAmount'=>'مبلغ تخفيف كل',
    'irounding'=>'گردن کردن فاکتور',
    'iposdaily'=>'فاکتور روزانه پوز',
    'creditsUsed'=>'اعتبار استفاده شده',
    'invoiceBalanceAmount'=>'مبلغ مانده فاكتور',
    'groupId'=>'ردیف گروه',
    'groupName'=>'نام گروه',
    'rate'=>'امتیاز',
    'groupCode'=>'كد گروه',
    'barcode'=>'باركد',
    'unitName'=>'نام واحد',
    'unit'=>' واحد',
    'numberLeft'=>'تعداد مانده',
    'factorAmount'=>'مبلغ فاکتور',
    'averageIn'=>'ميانگين في',
    'averageDiscount'=>'ميانگين تخفیف',
    'discountPercentage'=>'درصد تخفيف',
    'averageSell'=>'ميانگين فروش',
    'option'=>'تنظیمات',
    'reports'=>'گزارشات',
    'report'=>'گزارش',
    'createdDate'=>'تاریخ ثبت',
    'ready'=>'خوانده شده',
    'noReady'=>'خوانده نشده',
    'factorDate'=>'تاریخ فاکتور',
    'orgID'=>'ردیف مبدا',
    'UnitDesc'=>'توضیح واحد',
    'sumPrice'=>'جمع مبلغ',
    'viewAll'=>'نمایش همه',
    'all'=>'همه',
    'uploadFiles'=>'بارگزاری فایل',
    'uploadImage'=>'بارگزاری عکس',
    'attachFiles'=>'افزودن فایل',
    'attachedFiles'=>'فایل های متصل شده',
    'unitDesc'=>'توضیحات واحد',
    'reportSystem'=>'گزارشات سیستم',
    'noAdmin'=>'برگشتی های مدیر',
    'noRank'=>'برگشتی های جایگاه',
    'adminRank'=>'مدیر جایگاه',
    'addUser'=>'افزودن کاربر',
    'addAdmin'=>'افزودن مدیر',
    'addRank'=>'افزودن جایگاه',
    'select'=>'انتخاب',
    'responder'=>'پاسخ دهنده',
    'addResponder'=>'افزودن پاسخ دهنده',
    'backResponder'=>'برگشت به مدیر گروه',
    'backAdmin'=>'برگشت به مدیر',
    'typeAmount' => 'نوع پرداخت',
    'cash' => 'نقدی',
    'noCash' => 'غیرنقدی',
    'replyTo' => 'پاسخ به ',
    'replyToTask' => 'پاسخ به این کار',
    'factorNumber' => 'شماره فاکتور',
    'install' => 'نصب',
    'database' => 'پایگاه داده MYSQL',
    'databaseSqlServer' => 'پایگاه داده SQL SERVER',
    'usernameDb' => 'نام کاربری پایگاه داده',
    'passwordDb' => 'رمز عبور پایگاه داده',
    'urlRoot' => 'آدرس ریشه سایت',
    'urlSqlServer' => 'آدرس سرور SQL Server',
    'portSqlServer' => 'پورت سرور SQL Server',
    'fieldValue' => 'مقدار فیلد',
    'successPayment' => 'پرداخت شده',
    'successSendProduct' => 'محصول ارسال شده',
    'dangerSendProduct' => 'محصول ارسال نشده',
    'dangerPayment' => 'پرداخت نشده',
    'undefined' => 'ناشناخته',
    'pagesAdd' => 'افزودن صفحه',
    'pagesTrash' => 'صفحات حذف شده',
    'urlPage' => 'آدرس صفحه',
    'editElementor' => 'ویرایش با صفحه ساز',
    'deleteAll' => 'حذف همه',
    'admins' => 'مدیریت',
    'author' => 'نویسنده',
    'pageBuild' => 'ویرایش صفحه',
    'viewSite' => 'نمایش سایت',
    'place' => 'مکان',
    'icon' => 'آیکن',
    'footerMenu' => 'منوی پانوشت',
    'noGroup' => 'بدون گروه',
    'noCategory' => 'بدون دسته',
    'noMenu' => 'بدون منو',
    'trackingCode'=>'کد پیگیری',
    'priceAll'=>'مبلغ کل',
    'priceDiscount'=>'مبلغ تخفیف',
    'pricePay'=>'مبلغ پرداختی',
    'bankCode'=>'کد بانک',
    'bankName'=>'نام بانک',
    'message'=>'پیام',
    'changePosition'=>'تغییر وضعیت',
    'detailsOrders'=>'جزئیات سفارش',
    'plaque'=>'پلاک',
    'nameTransferee'=>'نام گیرنده',
    'nameProduct'=>'نام محصول',
    'count'=>'تعداد',
    'totalPrice'=>'مبلغ کل',
    'proccess'=>'در حال پردازش',
    'ticketID'=>'ردیف تیکت',
    'origin'=>'مبدا',
    'destination'=>'مقصد',
    'flightDate'=>'زمان پرواز',
    'flightNumber'=>'شماره پرواز',
    'airline'=>'airline',
    'price'=>'قیمت',
    'count'=>'تعداد',


    'warnNameRequired'=>'وارد کردن نام اجباری می باشد',
    'warnNameMax'=>'وارد کردن نام اجباری می باشد',
    'warnUrlRequired'=>'وارد کردن آدرس اجباری می باشد',
    'warnUrlMax'=>'وارد کردن آدرس اجباری می باشد',
    'warnTimeStartRequired'=>'وارد کردن زمان اجرا اجباری می باشد',
    'warnUrlUrl'=>'آدرس وارد شده نامعتبر می باشد',
    'warnUrlUnique'=>'آدرس وارد شده تکراری می باشد',
    'errorGateway'=>'خطایی در اتصال به درگاه رخ داده است.',
    'fileSuccessfullyDeleted' => 'فایل با موفقیت حذف شد',
    'addressNotFound' => 'آدرسی یافت نشد',
    'sendGateway' => 'در حال انتقال به درگاه پرداخت',
    'installDescription' => 'برای نصب اطلاعات مورد نظر را با دقت وارد نمایید',
    'errorSendEmail' => 'خطا در ارسال ایمیل',
    'loginDescription' => 'برای دیدن فعالیت ها وارد شوید.',
    'forgetPasswordDescription' => 'برای بازیابی رمز عبور ایمیل خود را وارد کنید ',
    'forgetPasswordAccount' => 'رمزتان را فراموش کرده اید؟',
    'noAccount' => 'حساب کاربری ندارید؟',
    'loginAccount' => 'ورود با حساب کاربری',
    'welcomeDashboard' => 'به بخش مدیریت خوش آمدید',
    'authTitleRequired' => 'وارد کردن عنوان اجباری می باشد',
    'authTitleMin' => 'تعداد کاراکتر عنوان کمتر از 3 حرف می باشد',
    'authDescriptionRequired' => 'وارد کردن توضیحات اجباری می باشد',
    'authColorRequired' => 'وارد کردن رنگ اجباری می باشد',
    'authUrlRequired' => 'وارد کردن آدرس اجباری می باشد',
    'authUrlUrl' => 'آدرس وارد شده نامعتبر می باشد',
    'authNameRequired' => 'وارد کردن نام اجباری می باشد',
    'authNameMin' => 'تعداد کاراکتر نام کمتر از 3 حرف می باشد',
    'authNameUnique' => 'نام مورد نظر قبلا ثبت شده است',
    'authLastNameRequired' => 'وارد کردن نام خانوادگی اجباری می باشد',
    'authLastNameMin' => 'تعداد کاراکتر نام خانوادگی کمتر از 3 حرف است',
    'authEmailRequired' => 'وارد کردن ایمیل اجباری می باشد',
    'authEmailEmail' => 'فرمت ایمیل وارد شده اشتباه است',
    'authEmailUnique' => 'ایمیل مورد نظر قبلا ثبت شده است',
    'authPhoneRequired' => 'وارد کردن شماره تلفن اجباری می باشد',
    'authPhoneRegex' => 'فرمت شماره تلفن وارد شده اشتباه است',
    'authPhoneUnique' => 'شماره تلفن مورد نظر قبلا ثبت شده است',
    'authPasswordRequired' => 'وارد کردن رمز عبور اجباری می باشد',
    'authConfirmPasswordRequired' => 'وارد کردن رمز عبور (مجدد) اجباری می باشد',
    'authPasswordRequiredWith' => 'وارد کردن رمز عبور و رمز عبور (مجدد) اجباری می باشد',
    'authPasswordSame' => 'رمزعبور و رمزعبور (مجدد) با هم مطابقت ندارند',
    'authPasswordMin' => 'حداقل تعداد رمزعبور 5 حرف می باشد',
    'authPageRequired' => 'انتخاب کردن یک صفحه اجباری است',
    'wornUsernameOrPassword' => 'نام کاربری یا رمز عبور اشتباه است',
    'wornEmailNotFound' => 'ایمیل مورد نظر یافت نشد',
    'wornPhoneNotFound' => 'شماره تلفن مورد نظر یافت نشد',
    'dataNotFound' => 'موردی یافت نشد',
    'wornUrlNotFound' => 'خطا آدرسی یافت نشد',
    'wornUserNotFound' => 'کاربری یافت نشد',
    'successAddUrl' => 'آدرس با موفقیت اضافه شد',
    'successUpdateUrl' => 'آدرس با موفقیت بروز شد',
    'successRecoveryUrl' => 'آدرس با موفقیت بازیابی شد',
    'successDeleteUrl' => 'آدرس با موفقیت حذف شد',
    'successUpdateProfile' => 'پروفایل با موفقیت بروز شد',
    'successUpdatePassword' => 'رمزعبور با موفقیت بروز شد',
    'successDeleteRoles' => 'دسترسی با موفقیت حذف شد',
    'errorDeleteRoles' => 'خطایی در حذف دسترسی رخ داده است ',
    'successAddRoles' => 'دسترسی با موفقیت اضافه شد',
    'successUpdateRoles' => 'دسترسی با موفقیت بروز شد',
    'successRecoveryUsers' => 'کاربر با موفقیت بازیابی شد',
    'successDeleteUsers' => 'کاربر با موفقیت حذف شد',
    'authReplyExists' => 'کاری برای پاسخ به آن یافت نشد',
    'warnUserExists' => 'کاربری یافت نشد',
    'warnUserRequired' => 'انتخاب کاربر اجباری می باشد',
    'warnPostRequired' => 'انتخاب محصول اجباری می باشد',
    'authParentRequired' => 'کار ارسالی برای پاسخ به آن نامعتبر می باشد',
    'errorDeleteUsers' => 'خطایی در حذف کاربر رخ داده است ',
    'uploadCompletedSuccessfully'=>'آپلود فایل با موفقیت انجام شد .',
    'anErrorOccurredWhileLoading'=>'خطایی در بارگزاری فایل رخ داده است .',
    'anErrorOccurredWhileSendingTheFile'=>'خطایی در ارسال فایل رخ داده است .',
    'theDesiredFileSizeIsTooMuch'=>'حجم فایل از حجم مجاز بیشتر می باشد .',
    'theSubmittedFileFormatIsNotAllowed'=>'فرمت فایل مورد نظر معتبر نمی باشد .',
    'saveCompletedSuccessfully'=>'ذخیره فایل با موفقیت انجام شد .',
    'fileNotFound'=>'فایلی یافت نشد.',
    'errorSendPhone'=>'در ارسال پیامک خطایی رخ داده است',
    'successSendPhone'=>'پیامک با موفقیت ارسال شد',
    'wornCodePasswordRecoveryNotFound'=>'کد وارد شده نامعتبر می باشد',
    'setNewPassword'=>'رمزعبور جدید را وارد نمایید',
    'wornEmptyMessage'=>'پیام خالی می باشد',
    'youAreInResponseMode'=>'شما در حالت پاسخ به نظر هستید ، برای خارج شدن از این حالت کلیک کنید',
    'wornRequiredAllFields'=>'وارد کردن همه موارد الزامی می باشد',
    'successInstall'=>'سیستم با موفقیت نصب شد .',
    'errorInstall'=>'در نصب سیستم خطایی رخ داده است ، لطفا دوباره امتحان کنید ',
    'successAddProduct' => 'محصول با موفقیت اضافه شد',
    'successUpdateProduct' => 'محصول با موفقیت به روز شد',
    'successDeleteProduct' => 'محصول با موفقیت حذف شد',
    'successRecoveryProduct' => 'محصول با موفقیت بازیابی شد',
    'errorDeleteProduct' => 'خطایی در حذف محصول رخ داده است',
    'warnProductNotFound'=>'محصول مورد نظر یافت نشد',
    'successAddOrders' => 'سفارش با موفقیت اضافه شد',
    'successUpdateOrders' => 'سفارش با موفقیت به روز شد',
    'successDeleteOrders' => 'سفارش با موفقیت حذف شد',
    'successRecoveryOrders' => 'سفارش با موفقیت بازیابی شد',
    'errorDeleteOrders' => 'خطایی در حذف سفارش رخ داده است',
    'warnOrdersNotFound'=>'سفارش مورد نظر یافت نشد',
    'warnPriceBuyRequired'=>'وارد کردن قیمت خرید اجباری است',
    'warnPriceSellRequired'=>'وارد کردن قیمت فروش اجباری است',
    'warnBrandRequired' => 'وارد کردن برند اجباری می باشد',
    'errorDeleteBrand' => 'خطایی در حذف برند رخ داده است ',
    'successAddBrand' => 'برند با موفقیت اضافه شد',
    'successUpdateBrand' => 'برند با موفقیت به روز شد',
    'successDeleteBrand' => 'برند با موفقیت حذف شد',
    'warnBrandNotFound'=>'برند مورد نظر یافت نشد',
    'warnChildBrandNotFound'=>'زیرمجموعه مورد نظر یافت نشد',
    'warnBrandExists' => 'برندی یافت نشد',
    'successAddCategory' => 'دسته با موفقیت اضافه شد',
    'successUpdateCategory' => 'دسته با موفقیت به روز شد',
    'successDeleteCategory' => 'دسته با موفقیت حذف شد',
    'errorDeleteCategory' => 'خطایی در حذف دسته رخ داده است',
    'warnCategoryNotFound'=>'دسته مورد نظر یافت نشد',
    'warnColorRequired' => 'وارد کردن رنگ اجباری می باشد',
    'warnCategoryRequired' => 'وارد کردن دسته اجباری می باشد',
    'warnCategoryExists' => 'دسته ای یافت نشد',
    'successAddMenu' => 'منو با موفقیت اضافه شد',
    'successUpdateMenu' => 'منو با موفقیت به روز شد',
    'successDeleteMenu' => 'منو با موفقیت حذف شد',
    'errorDeleteMenu' => 'خطایی در حذف منو رخ داده است',
    'warnMenuNotFound'=>'منو مورد نظر یافت نشد',
    'warnMenuRequired' => 'وارد کردن منو اجباری می باشد',
    'warnMenuExists' => 'منو ای یافت نشد',
    'warnSupplierRequired' => 'وارد کردن تامین کننده اجباری می باشد',
    'warnSupplierExists' => 'تامین کننده ای یافت نشد',
    'successAddSupplier' => 'تامین کننده با موفقیت اضافه شد',
    'successUpdateSupplier' => 'تامین کننده با موفقیت به روز شد',
    'successDeleteSupplier' => 'تامین کننده با موفقیت حذف شد',
    'successRecoverySupplier'=>'تامین کننده با موفقیت بازیابی شد',
    'errorDeleteSupplier'=>'خطایی در حذف تامین کننده رخ داده است',
    'warnAmountRequired'=>'مقدار را وارد کنید',
    'warnSupplierNotFound'=>'تامین کننده مورد نظر یافت نشد',
    'warnNameShopRequired'=>'وارد کردن نام فروشگاه اجباری می باشد',
    'warnAddressRequired'=>'وارد کردن آدرس اجباری می باشد',
    'warnPhoneRequired'=>'وارد کردن شماره تلفن اجباری می باشد',
    'warnEmailRequired'=>'وارد کردن ایمیل اجباری می باشد',
    'warnEmailEmail'=>'فرمت ایمیل اشتباه می باشد',
    'successAddColor' => 'رنگ با موفقیت اضافه شد',
    'successUpdateColor' => 'رنگ با موفقیت به روز شد',
    'successDeleteColor' => 'رنگ با موفقیت حذف شد',
    'warnColorNotFound'=>'رنگ مورد نظر یافت نشد',
    'errorColorFields' => 'خطایی در حذف رنگ رخ داده است',
    'successAddFields' => 'فیلد با موفقیت اضافه شد',
    'successUpdateFields' => 'فیلد با موفقیت به روز شد',
    'successDeleteFields' => 'فیلد با موفقیت حذف شد',
    'errorDeleteFields' => 'خطایی در حذف فیلد رخ داده است',
    'warnFieldsNotFound'=>'فیلد مورد نظر یافت نشد',
    'warnChildCategoryNotFound'=>'زیرمجموعه مورد نظر یافت نشد',
    'warnCommentRequired'=>'نظر مورد نظر یافت نشد',
    'wornCommentRequired' => 'نظر مورد نظر ناشناخته می باشد',
    'warnCommentMin' => 'حداقل تعداد نظر 3 کاراکتر می باشد',
    'successAddComment' => 'نظر با موفقیت اضافه شد',
    'successUpdateComment' => 'نظر با موفقیت به روز شد',
    'successReplayComment' => 'نظر با موفقیت پاسخ داده شد',
    'successDeleteComment' => 'نظر با موفقیت حذف شد',
    'successRecoveryComment' => 'نظر با موفقیت بازیابی شد',
    'errorDeleteComment' => 'خطایی در پاک کردن نظر رخ داده است',
    'wornCommentNotFound' => 'نظری یافت نشد',
    'warnSliderNotFound'=>'اسلایدر مورد نظر یافت نشد',
    'successAddSlider' => 'اسلایدر با موفقیت اضافه شد',
    'successUpdateSlider' => 'اسلایدر با موفقیت به روز شد',
    'successDeleteSlider' => 'اسلایدر با موفقیت حذف شد',
    'warnDiscountNotFound'=>'تخفیف مورد نظر یافت نشد',
    'successAddDiscount' => 'تخفیف با موفقیت اضافه شد',
    'successUpdateDiscount' => 'تخفیف با موفقیت به روز شد',
    'successDeleteDiscount' => 'تخفیف با موفقیت حذف شد',
    'warnFooterUrlNotFound'=>'پیوند پانوشته مورد نظر یافت نشد',
    'successAddFooterUrl' => 'پیوند پانوشته با موفقیت اضافه شد',
    'successUpdateFooterUrl' => 'پیوند پانوشته با موفقیت به روز شد',
    'successDeleteFooterUrl' => 'پیوند پانوشته با موفقیت حذف شد',
    'warnAdvertisingNotFound'=>'تبلیغ مورد نظر یافت نشد',
    'successAddAdvertising' => 'تبلیغ با موفقیت اضافه شد',
    'successUpdateAdvertising' => 'تبلیغ با موفقیت به روز شد',
    'successDeleteAdvertising' => 'تبلیغ با موفقیت حذف شد',
    'successUpdateOption' => 'تنظیمات با موفقیت به روز شد',

    'warnTitleRequired' => 'وارد کردن عنوان اجباری می باشد',
    'warnPlaceRequired' => 'وارد کردن مکان اجباری می باشد',
    'warnIconRequired' => 'وارد کردن آیکن اجباری می باشد',
    'warnUrlRequired' => 'وارد کردن آدرس اینترنتی اجباری می باشد',
    'warnTitleMin' => 'تعداد کاراکتر عنوان کمتر از 3 حرف می باشد',
    'warnRateRegex' => 'امتیاز را به صورت عددی وارد نمایید',
    'warnDescriptionRequired' => 'وارد کردن توضیحات اجباری می باشد',
    'deleteSelectedItems'=>'آیا آیتمهای انتخابی حذف شوند؟',
    'successAddPage'=>'صفحه با موفقیت افزوده شد',
    'successEditPage'=>'صفحه با موفقیت ویرایش شد',
    'errorEditPage'=>'خطایی در ویرایش صفحه رخ داده است',
    'successDeletePage'=>'صفحه با موفقیت حذف شد',
    'successRecoveryPage'=>'صفحه با موفقیت بازیابی شد',
    'errorPageNotFound'=>'صفحه ای یافت نشد',
    'confirmDelete'=>'آیا میخواهید حذف کنید؟',

    'footerText' => 'کلیه حقوق محفوظ است',
    'footerTime' => ' 1400-1401',
    'footerCreator' => 'طراحی توسط : گروه مهر',
    'footerCreatorUrl' => '#',
];
