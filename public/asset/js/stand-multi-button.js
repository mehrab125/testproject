
function trash() {
    $(".trashImage").click(function () {
        var src = $(this).parent().find('img').attr('src');
        $('#thumbnail').val($('#thumbnail').val().replace(src, '').replace(',,', ','));
        $(this).parent().remove();
    });
}
trash();

(function ($) {
    $.fn.filemanager = function (type, options) {
        type = type || 'file';

        this.on('click', function (e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
            var target_input = $('#' + $(this).data('input'));
            var target_preview = $('#' + $(this).data('preview'));
            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            window.SetUrl = function (items) {
                var file_path = items.map(function (item) {
                    return item.url;
                }).join(',');

                // set the value of the desired input to image url
                target_input.val(target_input.val() + ',' + file_path).trigger('change');

                // target_preview.html('');
                // set or change the preview image src
                items.forEach(function (item) {
                    var html = '<div class="item-upload">\n' +
                        '<img src="' + item.thumb_url + '" alt="image"/>\n' +
                        '<i class="fa fa-trash trashImage"></i>\n' +
                        '</div>';
                    target_preview.html(target_preview.html() + html);
                });
                // trigger change event
                // target_preview.trigger('change');
                trash();
            };

            return false;
        });
    }

})(jQuery);
