<textarea name="text" id="editor" class=" {{$class}}">
{{ $slot }}
</textarea>
@section("style")


@endsection
@section("script")
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/ckeditor/ckeditor.js')}}"></script>
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/ckeditor/config.js')}}"></script>
    <script>
        CKEDITOR.replace('editor', {
            language: 'fa',
            filebrowserImageBrowseUrl: '{{asset('/laravel-filemanager?type=Images')}}',
            {{--            filebrowserImageUploadUrl: '{{asset('/laravel-filemanager/upload?type=Images&_token=csrf_token()')}}',--}}
            filebrowserImageUploadUrl: '{{asset('/laravel-filemanager/upload?type=Images')}}',
            filebrowserBrowseUrl: '{{asset('/laravel-filemanager?type=Files')}}',
            {{--            filebrowserUploadUrl: '{{asset('/laravel-filemanager/upload?type=Files&_token=csrf_token()')}}',--}}
            filebrowserUploadUrl: '{{asset('/laravel-filemanager/upload?type=Files')}}',
        });

        $('#lfm').filemanager('image', {prefix: "/filemanager"});

    </script>
@endsection
