<style>

    #uploads {
        float: right;
        border: 2px dashed #ccc;
        border-radius: 20px;
        width: 100%;
        padding: 20px;
    }

    #uploads.highlight {
        border-color: purple;
    }

    #uploads #progressBar {
        float: right;
        height: 3px;
        width: 0;
        background-color: #0a0;
        border-radius: 5px;
        margin: 5px;
    }

    .gallery-uploads {
        margin-top: 10px;
        width: 100%;
        float: right;
    }

    .gallery-uploads img {
        width: 150px;
        border-radius: 5px;
    }

    #fileElem, .alert-uploads {
        display: none;
    }

    .btn-uploads {
        padding: 10px;
    }

    .item-upload {
        float: right;
        margin: 2px;
        width: 150px !important;
        height: 150px !important;
        position: relative;
    }

    .item-upload img {
        width: 100%;
        height: 100%;
        border: solid 2px #ccc;
    }

    .item-upload .trash {
        position: absolute !important;
        top: 10px !important;
        right: 10px !important;
        background-color: #000000dd !important;
        padding: 5px !important;
        border-radius: 50px !important;
        color: #fff !important;
        cursor: pointer !important;
        transition-property: all;
        transition-duration: 0.5s;
        left: auto !important;

    }

    .item-upload .trash:hover {
        color: #f90;
    }
</style>
<div class="form-group row">
    <div class="col-9">
        <div id="uploads">
            <input type="hidden" name="file" id="file" value="{{ isset($result['url'])?$result['url']:'' }}">
            <div id="progressBar"></div>
            <div class="alert alert-uploads"></div>
            <p class="col-form-label col-3 text-right">{{ __('text.uploadFiles') }}</p>
            <input type="file" id="fileElem" accept="image/*" onchange="handleFiles(this.files)">
            <label class="btn-uploads btn btn-info p-2 text-15 col-md-3 col-sm-6"
                   for="fileElem">{{ __("text.attachFiles") }}</label>
            <div class="gallery-uploads">
                <div class="item-upload"
                     item="{{ (isset($result['id']) && !empty($result['id'])?$result['id']:'0') }}">
                    <img
                        src="{{ (isset($result['url']) && !empty($result['url'])?$result['url']:\Illuminate\Support\Facades\URL::asset("asset/img/noImg.jpg")) }}"
                        alt="image" id="imageFile"/><i class="fa fa-trash trash"
                                                       style="display:{{ (isset($result) && !empty($result)?"block":"none") }}"></i>
                </div>
            </div>
        </div>
    </div>

</div>
@section("scriptUpload")
    <script>
        let dropArea = document.getElementById('uploads')

        ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, preventDefaults, false);
            document.body.addEventListener(eventName, preventDefaults, false);
        })

        ;['dragenter', 'dragover'].forEach(eventName => {
            dropArea.addEventListener(eventName, highlight, false);
        })

        ;['dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, unhighlight, false);
        });

        // Handle dropped files
        dropArea.addEventListener('drop', handleDrop, false);

        function preventDefaults(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        function highlight(e) {
            dropArea.classList.add('highlight');
        }

        function unhighlight(e) {
            dropArea.classList.remove('active');
        }

        function handleDrop(e) {
            var dt = e.dataTransfer;
            var files = dt.files;

            handleFiles(files);
        }

        let uploadProgress = [];
        let progressBar = $('#progressBar');

        function initializeProgress(numFiles) {
            progressBar.width('0%');
            uploadProgress = [];

            for (let i = numFiles; i > 0; i--) {
                uploadProgress.push(0);
            }
        }

        function updateProgress(fileNumber, percent) {
            uploadProgress[fileNumber] = percent;
            let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length;
            //console.debug('update', fileNumber, percent, total)
            progressBar.width(total + '%');
        }

        function handleFiles(files) {
            files = [...files];
            initializeProgress(files.length);
            files.forEach(uploadFile);
            files.forEach(previewFile);
        }

        function previewFile(file) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = function() {
                //var html ='<div class="item-upload"><img src="'+reader.result+'" alt="image" /></div>';
                //$('.gallery-uploads').html(html);
            };
        }

        function uploadFile(file, i) {
            $('.alert-uploads').removeClass('alert-success');
            $('.alert-uploads').removeClass('alert-danger');
            $('.alert-uploads').html('');
            $('.alert-uploads').show();
            var url = '{{ route("file.upload") }}';
            var form_data = new FormData();
            form_data.append('_token', '{{ csrf_token() }}');
            form_data.append('file', $('#fileElem')[0].files[0]);

            $.ajax({
                url: url,
                type: 'POST',
                data: form_data,
                cache: false,
                contentType: false,
                processData: false,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', function(event) {
                            if (event.lengthComputable) {
                                var value = parseInt((event.loaded / event.total) * 100);
                                updateProgress(i, value);
                                if (event.total == 100) {
                                    updateProgress(i, 0);
                                }
                            }
                        });
                    }
                    return myXhr;
                },
                success: function(result) {

                    if (result != null && result.type == 1) {
                        $('#imageFile').attr('src', result.url);
                        $('.alert-uploads').addClass('alert-success').html(result.message);
                        $('#progressBar').hide();

                        $('#file').val(result.id);
                        $('#imageFile').parent().attr('item', result.id);
                        $('.trash').show();
                        deleteImage();
                    } else {
                        $('.alert-uploads').addClass('alert-danger').html(result.message);
                        $('#progressBar').hide();
                    }
                },
                error: function(xhr) {
                    $('.alert-uploads').addClass('alert-danger').html(xhr.responseJSON.message);
                    $('#progressBar').hide();
                },
                beforeSend: function() {
                    updateProgress(i, 0);
                    $('#progressBar').show();
                }
            });
        }

        function deleteImage() {
            $('.trash').click(function() {
                var url = '{{ route("file.delete") }}';
                $('.alert-uploads').removeClass('alert-success');
                $('.alert-uploads').removeClass('alert-danger');
                $('.alert-uploads').html('');
                $('.alert-uploads').show();

                var file = $(this).parent();
                var id = file.attr('item');

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {'_token':'{{ csrf_token() }}','id':id},
                    success: function(result) {
                        if (result != null && result.type === 1) {
                            $('#file').val('');
                            $('#imageFile').attr('src', "{{ \Illuminate\Support\Facades\URL::asset("img/noImg.jpg") }}");
                            $('.trash').hide();
                            $('.alert-uploads').removeClass('alert-danger').html('');
                        } else {
                            $('.alert-uploads').addClass('alert-danger').html(result.message);
                        }
                        $('#progressBar').hide();
                    },
                    error: function(error) {
                        //$(".alert-uploads").addClass("alert-danger").html(xhr.responseJSON.message);
                        if(error.responseJSON.type ===404){
                            file.remove();
                        }
                        $('#progressBar').hide();
                        $('.alert-uploads').hide();
                    }
                });
            });
        }

        deleteImage();
    </script>
@endsection
