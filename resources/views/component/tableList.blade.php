<div class="card-body ">
<?PHP
$rank = \App\Exceptions\Auth::getRank();
?>
@if((isset($tableHeader) && !empty($tableHeader)) && isset($tableBody) && !empty($tableBody))
    <!--begin: Datatable-->
        <table class="table table-separate table-head-custom table-checkable rtl"
               @isset($id)id="kt_datatable{{$id}}" @else id="kt_datatable" @endisset>
            <thead>
            <tr>
                @foreach($tableHeader as $i => $item)
                    <th>{{$item}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($tableBody as $item)
                <tr>
                    <?PHP
                    $count = count($item);
                    ?>
                    @foreach($item as $field => $val)
                        @switch ($field)
                            @case($field == 'viewActive')
                            @break
                            @case($field == 'row')
                            <td>{{($val+1)}}</td>
                            @break
                            @case($field == 'position')
                            <td>
                                @component("component.label",['class'=>$val['class'],'text'=>$val['text']])@endcomponent
                            </td>
                            @break
                            @case($field == 'status')
                            <td>
                                @component("component.label",['class'=>$val['class'],'text'=>$val['text']])@endcomponent
                            </td>
                            @break
                            @case($field == 'option')
                            <td>
                                @switch ($val)
                                    @case("1")
                                    @component("component.btn",['url'=> route($page.".details",$item['id']),"class"=>"info","icon"=>'fal fa-info'])@endcomponent
                                    @component("component.btn",['url'=> route($page.".edit",$item['id']),"class"=>"warning","icon"=>'fal fa-edit'])@endcomponent
                                    @if ($rank != env("RANK_USER"))
                                        @if(isset($item['viewActive']))
                                            @component("component.btn",['url'=> route($page.".inactive",$item['id']),"class"=>"danger","icon"=>'fal fa-ban'])@endcomponent
                                        @endif
                                        @component("component.btn",['url'=> route($page.".delete",$item['id']),"class"=>"danger","icon"=>'fal fa-trash'])@endcomponent
                                    @endif
                                    @break
                                    @case("2")
                                    @component("component.btn",['url'=> route($page.".details",$item['id']),"class"=>"info","icon"=>'fal fa-info'])@endcomponent
                                    @component("component.btn",['url'=> route($page.".edit",$item['id']),"class"=>"warning","icon"=>'fal fa-edit'])@endcomponent
                                    @if ($rank != env("RANK_USER"))
                                        @if(isset($item['viewActive']))
                                            @component("component.btn",['url'=> route($page.".active",$item['id']),"class"=>"success","icon"=>'fal fa-check'])@endcomponent
                                        @endif
                                        @component("component.btn",['url'=> route($page.".delete",$item['id']),"class"=>"danger","icon"=>'fal fa-trash'])@endcomponent
                                    @endif
                                    @break
                                    @case("3")
                                    @if ($rank == env("RANK_ADMIN") || ($rank == env("RANK_FINANCIAL") && preg_match("#financial#",\Illuminate\Support\Facades\URL::current())))
                                        @component("component.btn",['url'=> route($page.".recovery",$item['id']),"class"=>"success","icon"=>'fal fa-undo'])@endcomponent
                                        @component("component.btn",['url'=> route($page.".trashDelete",$item['id']),"class"=>"danger","icon"=>'fal fa-trash'])@endcomponent
                                    @endif
                                    @break
                                    @case("4")
                                    @if ($rank != env("RANK_USER"))
                                        @component("component.btn",['url'=> route($page.".details",$item['id']),"class"=>"info","icon"=>'fal fa-info'])@endcomponent
                                    @endif
                                    @case("5")
                                    @if ($rank != env("RANK_USER") && isset($item['btn']))
                                        @foreach($item['btn'] as $btn )
                                         @component("component.btn",['url'=> $btn['route'],"class"=>$btn['class'],"icon"=>$btn['icon'],"text"=>(isset($btn['text'])?$btn['text']:'')])@endcomponent
                                        @endforeach
                                    @endif
                                    @break
                                @endswitch
                            </td>
                            @break
                            @case($field == 'btn')
                            @break
                            @case($field == 'noHash')
                                <td><?php echo ( $val ) ?></td>
                            @break
                            @default
                            @if ($field != "id")
                                <td>{{( $val )}}</td>
                            @endif
                            @break


                        @endswitch
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
        <!--end: Datatable-->
    @else
        @component("component.alert",['class'=>"danger"]) {{ __("text.noInformationّFund") }} @endcomponent
    @endif
</div>
@section("script")
    <script src="{{ \Illuminate\Support\Facades\URL::asset("js/paginations.js") }}"></script>
    <script src="{{ \Illuminate\Support\Facades\URL::asset("js/datatablesbundle.js") }}"></script>
@endsection
<?PHP
/*file:///C:/xampp/htdocs/laravel/crm/themeplate/dist/crud/datatables/basic/paginations.html*/
?>
