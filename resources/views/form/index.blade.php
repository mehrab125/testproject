@extends("master.master")

@section("title",__('text.form'))

@section("body")

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{ __('text.form') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">

                            @if (isset($result['message']))
                                @component('component.alert',['class'=>$result['class']])
                                    {{ $result['message'] }}
                                @endcomponent
                            @endif
                            @if (!empty($result['data'][0]))
                                <table class="table table-striped table-bordered table-hover dataTables-profile">
                                    <thead>
                                    <tr>
                                        <th>{{ __('text.id') }}</th>
                                        <th>{{ __('text.ticketID') }}</th>
                                        <th>{{ __('text.origin') }}</th>
                                        <th>{{ __('text.destination') }}</th>
                                        <th>{{ __('text.flightDate') }}</th>
                                        <th>{{ __('text.flightNumber') }}</th>
                                        <th>{{ __('text.airline') }}</th>
                                        <th>{{ __('text.price') }}</th>
                                        <th>{{ __('text.count') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($result['data'] as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->ticketID }}</td>
                                            <td>{{ $item->origin }}</td>
                                            <td>{{ $item->destination }}</td>
                                            <td>{{ $item->flightDate }}</td>
                                            <td>{{ $item->flightNumber }}</td>
                                            <td>{{ $item->airline }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->count }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>{{ __('text.id') }}</th>
                                        <th>{{ __('text.ticketID') }}</th>
                                        <th>{{ __('text.origin') }}</th>
                                        <th>{{ __('text.destination') }}</th>
                                        <th>{{ __('text.flightDate') }}</th>
                                        <th>{{ __('text.flightNumber') }}</th>
                                        <th>{{ __('text.airline') }}</th>
                                        <th>{{ __('text.price') }}</th>
                                        <th>{{ __('text.count') }}</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @else
                                @component("component.alert",['class'=>'danger'])
                                    {{ __("text.dataNotFound") }}
                                @endcomponent
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer')
    <!-- Data Tables -->
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/jquery.dataTables.js')}}">
    </script>
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/dataTables.bootstrap.js')}}">
    </script>
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/dataTables.responsive.js')}}">
    </script>
    <script
        src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/dataTables.tableTools.min.js')}}">
    </script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.dataTables-profile').dataTable({
                responsive: true,
                "order": [[ 0, "desc" ]],
                'tableTools': {
                    'sSwfPath': '{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf')}}'
                },
                'language': {
                    'sProcessing': '{{__('text.inProcessing')}}',
                    'sLengthMenu': '{{ __('text.viewData') }} _MENU_',
                    'sZeroRecords': '{{__('text.dataNotFound')}}',
                    'sInfo': '{{ __('text.view') }} _START_ {{ __('text.to') }} _END_ {{ __('text.ofTotal') }} _TOTAL_ {{__('text.item')}}',
                    'sInfoEmpty': '{{ __('text.zero') }}',
                    'sInfoFiltered': '({{ __('text.filterTotal') }} _MAX_ {{ __('text.item') }})',
                    'sInfoPostFix': '',
                    'sSearch': '{{ __('text.search') }} :',
                    'sUrl': '',
                    'oPaginate': {
                        'sFirst': '{{ __('text.first') }}',
                        'sPrevious': '{{ __('text.previous') }}',
                        'sNext': '{{ __('text.next') }}',
                        'sLast': '{{ __('text.last') }}'
                    }
                }
            });
        });
    </script>
@endsection
