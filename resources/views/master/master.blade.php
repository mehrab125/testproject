<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield("title")</title>
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/css/bootstrap.rtl.min.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <!-- Gritter -->
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/css/animate.css') }}" rel="stylesheet">
    <link href="{{ \Illuminate\Support\Facades\URL::asset('asset/css/style.rtl.css') }}" rel="stylesheet">
    @yield("header")
</head>
<body>
<div id="wrapper">
    @include("master.nav")
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-left">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">{{ __('text.welcomeDashboard') }}</span>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-primary">5</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">

                            <li>
                                    <div>
                                        <i class="fa fa-circle-o fa-fw"></i> test
                                        <span class="pull-left text-muted small">1400/01/01</span>
                                    </div>
                            </li>
                                <li class="divider"></li>

                             <li>
                                <div class="text-center link-block">
                                    <a href="{{ route('index.index') }}">
                                        <strong>{{ __('text.viewAll') }}</strong>
                                        <i class="fa fa-angle-left fa-lg fa-fw"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ env('APP_URL') }}" target="_blacnk">
                            <i class="fa fa-globe"></i> {{ __('text.viewSite') }}
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        @yield("body")

        <div class="row">
            <div class="col-lg-12">
                <div class="footer">
                    <div>
                        <strong>
                            {{ __('text.footerText') }}</strong> {{ __('text.footerTime') }} &copy;<a href="{{ __('text.footerCreatorUrl') }}" target="_blank">{{ __('text.footerCreator') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/jquery-2.1.1.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/bootstrap.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Flot -->
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/rada.js') }}"></script>
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ \Illuminate\Support\Facades\URL::asset('asset/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
@yield('footer')


</body>
</html>
