<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="img-circle"
                             src="{{ \Illuminate\Support\Facades\URL::asset('asset/img/user.jpg') }}" width="80"/>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                       <span class="clear">
                           <span class="block m-t-xs">
                               <strong
                                   class="font-bold">Name User</strong>
                           </span>
                           <span class="text-muted text-xs block">Admin<b class="caret"></b></span>
                       </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInLeft m-t-xs">
                        <li><a href="#">{{ __('text.profile') }}</a></li>
                        <li><a href="#">{{ __('text.changePassword') }}</a></li>
                        <li class="divider"></li>
                        <li><a href="#">{{ __('text.logout') }}</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    Name Site
                </div>
            </li>
            <li class="{{ \Illuminate\Support\Facades\URL::current()==route('index.index')?'active':'' }}">
                <a href="{{ route('index.index') }}">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label">{{ __('text.dashboard') }}</span>
                </a>
            </li>
            <li class="{{ \Illuminate\Support\Facades\URL::current()==route('url.index') || preg_match('#url#',\Illuminate\Support\Facades\URL::current())?'active':'' }}">
                <a href="javascript::">
                    <i class="fa fa-database"></i>
                    <span class="nav-label">{{ __('text.url') }}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{(\Illuminate\Support\Facades\URL::current()==route('url.index'))  ?'active':'' }}">
                        <a href="{{route('url.index')}}">{{ __('text.url') }}</a>
                    </li>
                    <li class="{{(\Illuminate\Support\Facades\URL::current()==route('url.add'))  ?'active':'' }}">
                        <a href="{{route('url.add')}}">{{ __('text.addUrl') }}</a>
                    </li>
                </ul>
            </li>
            <li class="{{ \Illuminate\Support\Facades\URL::current()==route('form.index') || preg_match('#form#',\Illuminate\Support\Facades\URL::current())?'active':'' }}">
                <a href="javascript::">
                    <i class="fa fa-database"></i>
                    <span class="nav-label">{{ __('text.form') }}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{(\Illuminate\Support\Facades\URL::current()==route('form.index'))  ?'active':'' }}">
                        <a href="{{route('form.index')}}">{{ __('text.form') }}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
