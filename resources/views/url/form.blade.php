@extends("master.master")
@if (isset($result['data']) && !empty($result['data']))
    @section("title",__('text.editUrl'))
@else
    @section("title",__('text.addUrl'))
@endif

@section("body")
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            @if (isset($result['data']) && !empty($result['data']))
                                {{ __('text.editUrl') }}
                            @else
                                {{ __('text.addUrl') }}
                            @endif
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            @if (isset($result['message']))
                                @component('component.alert',['class'=>$result['class']])
                                    {{ $result['message'] }}
                                @endcomponent
                            @endif

                            @if (isset($result['data']))
                                @if (empty($result['data']))
                                    @component("component.alert",['class'=>'danger'])
                                        {{ __("text.dataNotFound") }}
                                    @endcomponent
                                @endif
                                {!! Form::model($result['data'],['route'=>['url.editPr',$result['data']->id],'method'=>'post']) !!}
                            @else
                                {!! Form::open(['route'=>['url.addPr'],'method'=>'post']) !!}
                            @endif

                            <div class="form-group">
                                {!! Form::label("name",__('text.name')) !!}
                                {!! Form::text("name",null,['class'=>'form-control','placeholder'=>__('text.name'),'required'=>'required']) !!}
                                @if ($errors->has('name'))
                                    <span class="error text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::label("url",__('text.url')) !!}
                                {!! Form::text("url",null,['class'=>'form-control','placeholder'=>__('text.url'),'required'=>'required']) !!}
                                @if ($errors->has('url'))
                                    <span class="error text-danger">{{ $errors->first('url') }}</span>
                                @endif
                            </div>
{{--                            <div class="form-group">--}}
{{--                                {!! Form::label("timeStart",__('text.timeStart')) !!}--}}
{{--                                {!! Form::text("timeStart",null,['class'=>'form-control','placeholder'=>__('text.timeStart'),'required'=>'required']) !!}--}}
{{--                                @if ($errors->has('timeStart'))--}}
{{--                                    <span class="error text-danger">{{ $errors->first('timeStart') }}</span>--}}
{{--                                @endif--}}
{{--                            </div>--}}

                            @if (isset($result['data']) && !empty($result['data']))
                                {!! Form::submit(__('text.editUrl'),['class'=>'btn btn-primary block full-width m-b']) !!}
                            @else
                                {!! Form::submit(__('text.addUrl'),['class'=>'btn btn-primary block full-width m-b']) !!}

                            @endif
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section("header")
            <link href="{{\Illuminate\Support\Facades\URL::asset('asset/css/plugins/switchery/switchery.min.css')}}"
                  rel="stylesheet"/>
            <link href="{{\Illuminate\Support\Facades\URL::asset('asset/css/plugins/chosen/chosen.css')}}"
                  rel="stylesheet"/>

        @endsection
        @section("footer")
            <script
                src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/switchery/switchery.min.js')}}"></script>
            <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/stand-alone-button.js')}}"></script>
            <script
                src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/chosen/chosen.jquery.js')}}"></script>

            <script>
                $('#lfm').filemanager('image', {prefix: "/filemanager"});

                $('.chosen-select').chosen();
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                elems.forEach(function (html) {
                    var switchery = new Switchery(html);
                });
            </script>
    @yield('scriptUpload')
@endsection
