@extends("master.master")

@section("title",__('text.url'))

@section("body")

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{ __('text.url') }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">

                            @if (isset($result['message']))
                                @component('component.alert',['class'=>$result['class']])
                                    {{ $result['message'] }}
                                @endcomponent
                            @endif
                            @if (!empty($result['data'][0]))
                                <table class="table table-striped table-bordered table-hover dataTables-profile">
                                    <thead>
                                    <tr>
                                        <th>{{ __('text.id') }}</th>
                                        <th>{{ __('text.name') }}</th>
                                        <th>{{ __('text.url') }}</th>
{{--                                        <th>{{ __('text.timeStart') }}</th>--}}
                                        <th>{{ __('text.lastTimeStart') }}</th>
                                        <th>{{ __('text.options') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($result['data'] as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>@component('component.btn',['class'=>'info','text'=>__('text.url'),'url'=>$item->url])@endcomponent</td>
{{--                                            <td>{{ $item->timeStart }}</td>--}}
                                            <td>{{ (!empty($item->lastTimeStart)?\App\Exceptions\Handler::timestampToJalali($item->lastTimeStart):'') }}</td>
                                            <td>
                                                @component('component.btn',['url'=>route("url.edit",$item->id),'class'=>'warning','icon'=>'fa fa-edit','text'=>__('text.edit')])@endcomponent
                                                @component('component.btn',['url'=>route("url.delete",$item->id),'class'=>'danger','icon'=>'fa fa-trash','text'=>__('text.delete')])@endcomponent
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>{{ __('text.id') }}</th>
                                        <th>{{ __('text.name') }}</th>
                                        <th>{{ __('text.url') }}</th>
{{--                                        <th>{{ __('text.timeStart') }}</th>--}}
                                        <th>{{ __('text.lastTimeStart') }}</th>
                                        <th>{{ __('text.options') }}</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @else
                                @component("component.alert",['class'=>'danger'])
                                    {{ __("text.dataNotFound") }}
                                @endcomponent
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer')
    <!-- Data Tables -->
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/jquery.dataTables.js')}}">
    </script>
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/dataTables.bootstrap.js')}}">
    </script>
    <script src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/dataTables.responsive.js')}}">
    </script>
    <script
        src="{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/dataTables.tableTools.min.js')}}">
    </script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {
            $('.dataTables-profile').dataTable({
                responsive: true,
                "order": [[ 0, "desc" ]],
                'tableTools': {
                    'sSwfPath': '{{\Illuminate\Support\Facades\URL::asset('asset/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf')}}'
                },
                'language': {
                    'sProcessing': '{{__('text.inProcessing')}}',
                    'sLengthMenu': '{{ __('text.viewData') }} _MENU_',
                    'sZeroRecords': '{{__('text.dataNotFound')}}',
                    'sInfo': '{{ __('text.view') }} _START_ {{ __('text.to') }} _END_ {{ __('text.ofTotal') }} _TOTAL_ {{__('text.item')}}',
                    'sInfoEmpty': '{{ __('text.zero') }}',
                    'sInfoFiltered': '({{ __('text.filterTotal') }} _MAX_ {{ __('text.item') }})',
                    'sInfoPostFix': '',
                    'sSearch': '{{ __('text.search') }} :',
                    'sUrl': '',
                    'oPaginate': {
                        'sFirst': '{{ __('text.first') }}',
                        'sPrevious': '{{ __('text.previous') }}',
                        'sNext': '{{ __('text.next') }}',
                        'sLast': '{{ __('text.last') }}'
                    }
                }
            });
        });
    </script>
@endsection
