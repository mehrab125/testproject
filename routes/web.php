<?php

use App\Http\Controllers\FormContorller;
use App\Http\Controllers\UrlContorller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FormContorller::class,'index'])->name('index.index');
Route::prefix('form')->group(function (){
    Route::get('index',[FormContorller::class,'index'])->name('form.index');
    Route::get('add',[FormContorller::class,'add'])->name('form.add');
    Route::get('trash',[FormContorller::class,'trash'])->name('form.trash');

    Route::post('add',[FormContorller::class,'addPr'])->name('form.addPr');
    Route::patch('edit',[FormContorller::class,'editPr'])->name('form.editPr');
    Route::delete('delete',[FormContorller::class,'deletePr'])->name('form.deletePr');
});
Route::prefix('url')->group(function (){
    Route::get('index',[UrlContorller::class,'index'])->name('url.index');
    Route::get('add',[UrlContorller::class,'add'])->name('url.add');
    Route::get('edit/{id}',[UrlContorller::class,'edit'])->name('url.edit');
    Route::get('delete/{id}',[UrlContorller::class,'delete'])->name('url.delete');
    Route::get('trash',[UrlContorller::class,'trash'])->name('url.trash');

    Route::post('add',[UrlContorller::class,'addPr'])->name('url.addPr');
    Route::post('edit/{id}',[UrlContorller::class,'editPr'])->name('url.editPr');
});
